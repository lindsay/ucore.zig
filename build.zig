const std = @import("std");
const ArrayList = std.ArrayList;

pub fn build(b: *std.Build) void {
    // const modules = .
    const cIncludes = [_][]const u8{};
    const assemblyFiles = [_][]const u8{};

    const target = std.zig.CrossTarget{
        .cpu_arch = .riscv64,
        .os_tag = std.Target.Os.Tag.freestanding,
        .abi = std.Target.Abi.none,
    };

    const optimize = b.standardOptimizeOption(.{
        .preferred_optimize_mode = .ReleaseSmall,
    });

    const elf = b.addExecutable(.{
        .name = "ucore.zig",
        .root_source_file = .{ .path = "src/kmain.zig" },
        .target = target,
        .optimize = optimize,
    });

    for (cIncludes) |i| {
        elf.addIncludePath(.{ .path = i });
    }
    for (assemblyFiles) |i| {
        elf.addAssemblyFile(.{ .path = i });
    }

    elf.setLinkerScript(.{ .path = "kernel.ld" });
    elf.code_model = .medium;
    const copy_elf = b.addInstallArtifact(elf, .{});
    b.default_step.dependOn(&copy_elf.step);

    const bin = b.addObjCopy(elf.getEmittedBin(), .{
        .format = .bin,
    });
    bin.step.dependOn(&elf.step);

    const copy_bin = b.addInstallBinFile(bin.getOutput(), "ucore.zig.bin");
    b.default_step.dependOn(&copy_bin.step);

    var qemu_args_al = ArrayList([]const u8).init(b.allocator);
    defer qemu_args_al.deinit();

    switch (target.getCpuArch()) {
        .riscv64 => qemu_args_al.append("qemu-system-riscv64") catch unreachable,
        else => unreachable,
    }

    qemu_args_al.appendSlice(&([_][]const u8{
        "-machine",   "virt",
        "-bios",      "./fw_jump.bin",
        "-serial",    "mon:stdio",
        "-device",    "loader,file=./zig-out/bin/ucore.zig.bin,addr=0x80200000",
        "-nographic",
    })) catch unreachable;

    var qemu_args = qemu_args_al.toOwnedSlice() catch unreachable;
    const qemu_exec = b.addSystemCommand(qemu_args);

    qemu_exec.step.dependOn(b.default_step);
    const run_step = b.step("qemu", "Run the kernel with QEMU");
    run_step.dependOn(&qemu_exec.step);
}
