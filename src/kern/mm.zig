const list = @import("../libs/list.zig");
const rv = @import("../libs/riscv.zig");

const PageFlagBit = .{
    .reserved = 0,
    .property = 1,
};

pub const Page = struct {
    ref: isize = 0,
    flags: u64 = 0,
    property: usize = 0,
    pageLink: list.ListEntry,
    pub fn setReserved(p: *Page) void {
        p.flags |= 1 << PageFlagBit.reserved;
    }
    pub fn getReserved(p: *Page) bool {
        return (p.flags & (1 << PageFlagBit.reserved)) != 0;
    }
    pub fn getProperty(p: *Page) bool {
        return (p.flags & (1 << PageFlagBit.property)) != 0;
    }
    pub fn setProperty(p: *Page) void {
        p.flags |= (1 << PageFlagBit.property);
    }
    pub fn clearProperty(p: *Page) void {
        p.flags &= ~@as(usize, (1 << PageFlagBit.property));
    }
};

pub inline fn PPN(la: usize) usize {
    return la >> rv.MMLayout.PGSHIFT;
}

pub const FreeArea = struct {
    nrFree: usize,
    freeList: list.ListEntry,
};
