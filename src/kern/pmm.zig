const std = @import("std");
const math = std.math;
const mm = @import("./mm.zig");
const rv = @import("../libs/riscv.zig");
const stdio = @import("../libs/stdio.zig");

const PMMError = error{
    OutOfMemory,
    InvalidRequest,
    PageRefree,
};
extern const end: usize;
const nbase = rv.MMLayout.DRAM_BASE / rv.MMLayout.PGSIZE;
extern const boot_page_table_sv39: usize;
var npage: usize = 0;
var pages: [*]mm.Page = undefined;
var freeArea: mm.FreeArea = undefined;

const MemManager = struct {
    init: *const fn () error{}!void = undefined,
    initMemMap: *const fn (*mm.Page, usize) PMMError!void = undefined,
    allocatePage: *const fn (usize) PMMError!*mm.Page = undefined,
    freePages: *const fn (*mm.Page, usize) PMMError!void = undefined,
    nrFreePages: *const fn () usize = undefined,
    check: *const fn () PMMError!void = undefined,
};

pub var pageManager = MemManager{};
pub const pageManagerType = enum {
    defaultManager,
    bestFitManager,
};

inline fn pa2page(pa: usize) !*mm.Page {
    const pn = mm.PPN(pa);
    if (pn > npage) {
        return PMMError.OutOfMemory;
    } else {
        return &(pages[pn - nbase]);
    }
}

pub fn init(t: pageManagerType) !void {
    // init manager
    switch (t) {
        .defaultManager => {
            pageManager.init = defaultInit;
            pageManager.initMemMap = defaultInitMemMap;
            pageManager.allocatePage = defaultAllocatePage;
            pageManager.freePages = defaultFreePages;
            pageManager.nrFreePages = defaultNrFreePages;
            pageManager.check = defaultCheck;
        },
        .bestFitManager => {
            pageManager.init = defaultInit;
            pageManager.initMemMap = defaultInitMemMap;
            pageManager.allocatePage = bestFitAllocatePage;
            pageManager.freePages = defaultFreePages;
            pageManager.nrFreePages = defaultNrFreePages;
            pageManager.check = defaultCheck;
        },
    }

    try pageManager.init();

    // init pages
    // 数量不对？
    const maxpa: usize = maxpa: {
        if (rv.MMLayout.PHYSICAL_MEMORY_END > rv.MMLayout.KERNTOP) {
            break :maxpa rv.MMLayout.KERNTOP;
        } else {
            break :maxpa rv.MMLayout.PHYSICAL_MEMORY_END;
        }
    };

    npage = (maxpa / rv.MMLayout.PGSIZE) - 1;

    pages = @as([*]mm.Page, @ptrFromInt(rv.MMLayout.PGSIZE * try math.divCeil(usize, @intFromPtr(&end), rv.MMLayout.PGSIZE)));
    for (nbase..npage, 0..) |_, i| {
        (&pages[i]).setReserved();
    }

    // 初始化内核页映射

    // 内核剩余的页
    const freemem = rv.PADDR(@intFromPtr(pages) + @sizeOf(mm.Page) * (npage - nbase + 1));
    const mem_begin = rv.MMLayout.PGSIZE * try math.divCeil(usize, freemem, rv.MMLayout.PGSIZE);
    const mem_end = rv.MMLayout.PGSIZE * try math.divFloor(usize, rv.MMLayout.PHYSICAL_MEMORY_END, rv.MMLayout.PGSIZE);
    if (freemem < mem_end) {
        try pageManager.initMemMap(try pa2page(mem_begin), (mem_end - mem_begin) / rv.MMLayout.PGSIZE);
    }

    try pageManager.check();

    // const p = pageManager.allocatePage(1) catch |err| ct: {
    //     stdio.print("{any}\n", .{err});
    //     break :ct null;
    // };

    // if (p) |rp| {
    //     stdio.print("{b} {any} {any}\n", .{ rp.flags, !rp.getReserved(), !rp.getProperty() });
    //     pageManager.freePages(rp, 1) catch |err| {
    //         stdio.print("{any}\n", .{err});
    //     };
    // }

    stdio.print(
        \\================= Memory Infomartion ======================
        \\End address: 0x{x}
        \\First page instance address: 0x{x}
        \\Last page instance address: 0x{x}
        \\Free memory start: 0x{x}
        \\First page number: {d}
        \\Last page number: {d}
        \\Reserved pages: {d}
        \\satp virtual address: 0x{x}
        \\satp physical address: 0x{x}
        \\Free pages: {d}
        \\==========================================================
        \\
    , .{
        maxpa,
        @intFromPtr(pages),
        @intFromPtr(pages + npage - nbase),
        mem_begin,
        nbase,
        npage,
        npage - nbase,
        @intFromPtr(&boot_page_table_sv39),
        rv.PADDR(@intFromPtr(&boot_page_table_sv39)),
        pageManager.nrFreePages(),
    });
}

pub fn defaultInit() error{}!void {
    freeArea.freeList.init();
}

pub fn defaultInitMemMap(base: *mm.Page, size: usize) PMMError!void {
    if (size < 0) {
        return PMMError.InvalidRequest;
    }

    const p = @as([*]mm.Page, @ptrCast(base));

    for (0..size) |i| {
        (&p[i]).flags = 0;
        (&p[i]).property = 0;
        (&p[i]).ref = 0;
    }
    base.property = size;
    base.setProperty();
    freeArea.nrFree += size;
    if (freeArea.freeList.isEmpty()) {
        freeArea.freeList.addAfter(&base.pageLink);
    } else {
        var le = freeArea.freeList.next;
        while (le != &freeArea.freeList) {
            var page = @fieldParentPtr(mm.Page, "pageLink", le);
            if (@intFromPtr(base) < @intFromPtr(page)) {
                le.addBefore(&base.pageLink);
                break;
            } else if (le.next == &freeArea.freeList) {
                le.addAfter(&base.pageLink);
            }
            le = le.next;
        }
    }
}

pub fn defaultAllocatePage(size: usize) PMMError!*mm.Page {
    if (size <= 0) {
        return PMMError.InvalidRequest;
    } else if (size > freeArea.nrFree) {
        return PMMError.InvalidRequest;
    }
    var page: ?*mm.Page = null;
    var le = freeArea.freeList.next;

    while (le != &freeArea.freeList) {
        var p = @fieldParentPtr(mm.Page, "pageLink", le);
        if (p.property > size) {
            page = p;
            break;
        }
        le = le.next;
    }

    if (page) |rpage| {
        var prev = rpage.pageLink.prev;
        rpage.pageLink.delete();

        if (rpage.property > size) {
            var p = &(@as([*]mm.Page, @ptrCast(rpage))[size]);

            p.property = rpage.property - size;
            p.setProperty();

            prev.addAfter(&(p.pageLink));
        }
        freeArea.nrFree -= size;
        rpage.clearProperty();
    } else {
        return PMMError.OutOfMemory;
    }

    return if (page) |p| p else PMMError.OutOfMemory;
}

pub fn bestFitAllocatePage(size: usize) PMMError!*mm.Page {
    if (size <= 0) {
        return PMMError.InvalidRequest;
    } else if (size > freeArea.nrFree) {
        return PMMError.InvalidRequest;
    }
    var page: ?*mm.Page = null;
    var le = freeArea.freeList.next;

    while (le != &freeArea.freeList) {
        var p = @fieldParentPtr(mm.Page, "pageLink", le);
        if (p.property > size) {
            if (page) |rp| {
                if (rp.property < p.property) {
                    le = le.next;
                    continue;
                }
            }
            page = p;
        }
        le = le.next;
    }

    if (page) |rpage| {
        var prev = rpage.pageLink.prev;
        rpage.pageLink.delete();

        if (rpage.property > size) {
            var p = &(@as([*]mm.Page, @ptrCast(rpage))[size]);

            p.property = rpage.property - size;
            p.setProperty();

            prev.addAfter(&(p.pageLink));
        }
        freeArea.nrFree -= size;
        rpage.clearProperty();
    } else {
        return PMMError.OutOfMemory;
    }

    return if (page) |p| p else PMMError.OutOfMemory;
}

pub fn defaultFreePages(b: *mm.Page, size: usize) PMMError!void {
    if (size <= 0) {
        return PMMError.InvalidRequest;
    }
    var base = b;
    var fpages = @as([*]mm.Page, @ptrCast(base));
    for (0..size) |i| {
        if (fpages[i].getProperty()) {
            return PMMError.PageRefree;
        } else if (fpages[i].getReserved()) {
            return PMMError.PageRefree;
        }
        (&fpages[i]).flags = 0;
        (&fpages[i]).ref = 0;
    }
    base.property = size;
    base.setProperty();
    freeArea.nrFree += size;
    if (freeArea.freeList.isEmpty()) {
        freeArea.freeList.addAfter(&base.pageLink);
    } else {
        var le = freeArea.freeList.next;
        while (le != &freeArea.freeList) {
            var p = @fieldParentPtr(mm.Page, "pageLink", le);
            if (@intFromPtr(base) < @intFromPtr(p)) {
                le.addBefore(&base.pageLink);
                break;
            } else if (le.next == &freeArea.freeList) {
                le.addAfter(&base.pageLink);
            }
            le = le.next;
        }
    }

    const prev = base.pageLink.prev;
    if (prev != &freeArea.freeList) {
        const p = @fieldParentPtr(mm.Page, "pageLink", prev);
        if (@intFromPtr(p) + (@sizeOf(mm.Page) * p.property) == @intFromPtr(base)) {
            p.property += base.property;
            base.clearProperty();
            base.pageLink.delete();
            base = p;
        }
    }
    const next = base.pageLink.next;
    if (next != &freeArea.freeList) {
        if (@intFromPtr(base) + (@sizeOf(mm.Page) * base.property) == @intFromPtr(base)) {
            const p = @fieldParentPtr(mm.Page, "pageLink", next);
            base.property += p.property;
            p.clearProperty();
            p.pageLink.delete();
        }
    }
}
pub fn defaultNrFreePages() usize {
    return freeArea.nrFree;
}
pub fn defaultCheck() !void {
    return;
}
