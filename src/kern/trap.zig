const std = @import("std");
const fmt = std.fmt;
const rv = @import("../libs/riscv.zig");
const stdio = @import("../libs/stdio.zig");
const clock = @import("../kern/clock.zig");

comptime {
    asm (fmt.comptimePrint(
            \\.macro SAVE_ALL
            \\csrw sscratch, sp
            \\addi sp, sp, -36 * {[regbytes]d}
            \\{[store]s} x0, 0*{[regbytes]d}(sp)
            \\{[store]s} x1, 1*{[regbytes]d}(sp)
            \\{[store]s} x3, 3*{[regbytes]d}(sp)
            \\{[store]s} x4, 4*{[regbytes]d}(sp)
            \\{[store]s} x5, 5*{[regbytes]d}(sp)
            \\{[store]s} x6, 6*{[regbytes]d}(sp)
            \\{[store]s} x7, 7*{[regbytes]d}(sp)
            \\{[store]s} x8, 8*{[regbytes]d}(sp)
            \\{[store]s} x9, 9*{[regbytes]d}(sp)
            \\{[store]s} x10, 10*{[regbytes]d}(sp)
            \\{[store]s} x11, 11*{[regbytes]d}(sp)
            \\{[store]s} x12, 12*{[regbytes]d}(sp)
            \\{[store]s} x13, 13*{[regbytes]d}(sp)
            \\{[store]s} x14, 14*{[regbytes]d}(sp)
            \\{[store]s} x15, 15*{[regbytes]d}(sp)
            \\{[store]s} x16, 16*{[regbytes]d}(sp)
            \\{[store]s} x17, 17*{[regbytes]d}(sp)
            \\{[store]s} x18, 18*{[regbytes]d}(sp)
            \\{[store]s} x19, 19*{[regbytes]d}(sp)
            \\{[store]s} x20, 20*{[regbytes]d}(sp)
            \\{[store]s} x21, 21*{[regbytes]d}(sp)
            \\{[store]s} x22, 22*{[regbytes]d}(sp)
            \\{[store]s} x23, 23*{[regbytes]d}(sp)
            \\{[store]s} x24, 24*{[regbytes]d}(sp)
            \\{[store]s} x25, 25*{[regbytes]d}(sp)
            \\{[store]s} x26, 26*{[regbytes]d}(sp)
            \\{[store]s} x27, 27*{[regbytes]d}(sp)
            \\{[store]s} x28, 28*{[regbytes]d}(sp)
            \\{[store]s} x29, 29*{[regbytes]d}(sp)
            \\{[store]s} x30, 30*{[regbytes]d}(sp)
            \\{[store]s} x31, 31*{[regbytes]d}(sp)
            \\csrrw s0, sscratch, x0
            \\csrr s1, sstatus
            \\csrr s2, sepc
            \\csrr s3, stval
            \\csrr s4, scause
            \\{[store]s} s0, 2*{[regbytes]d}(sp)
            \\{[store]s} s1, 32*{[regbytes]d}(sp)
            \\{[store]s} s2, 33*{[regbytes]d}(sp)
            \\{[store]s} s3, 34*{[regbytes]d}(sp)
            \\{[store]s} s4, 35*{[regbytes]d}(sp)
            \\.endm
            \\
            \\.macro LOAD_ALL
            \\{[load]s} s1, 32*{[regbytes]d}(sp)
            \\{[load]s} s2, 33*{[regbytes]d}(sp)
            \\csrw sstatus, s1
            \\csrw sepc, s2
            \\{[load]s} x1, 1*{[regbytes]d}(sp)
            \\{[load]s} x3, 3*{[regbytes]d}(sp)
            \\{[load]s} x4, 4*{[regbytes]d}(sp)
            \\{[load]s} x5, 5*{[regbytes]d}(sp)
            \\{[load]s} x6, 6*{[regbytes]d}(sp)
            \\{[load]s} x7, 7*{[regbytes]d}(sp)
            \\{[load]s} x8, 8*{[regbytes]d}(sp)
            \\{[load]s} x9, 9*{[regbytes]d}(sp)
            \\{[load]s} x10, 10*{[regbytes]d}(sp)
            \\{[load]s} x11, 11*{[regbytes]d}(sp)
            \\{[load]s} x12, 12*{[regbytes]d}(sp)
            \\{[load]s} x13, 13*{[regbytes]d}(sp)
            \\{[load]s} x14, 14*{[regbytes]d}(sp)
            \\{[load]s} x15, 15*{[regbytes]d}(sp)
            \\{[load]s} x16, 16*{[regbytes]d}(sp)
            \\{[load]s} x17, 17*{[regbytes]d}(sp)
            \\{[load]s} x18, 18*{[regbytes]d}(sp)
            \\{[load]s} x19, 19*{[regbytes]d}(sp)
            \\{[load]s} x20, 20*{[regbytes]d}(sp)
            \\{[load]s} x21, 21*{[regbytes]d}(sp)
            \\{[load]s} x22, 22*{[regbytes]d}(sp)
            \\{[load]s} x23, 23*{[regbytes]d}(sp)
            \\{[load]s} x24, 24*{[regbytes]d}(sp)
            \\{[load]s} x25, 25*{[regbytes]d}(sp)
            \\{[load]s} x26, 26*{[regbytes]d}(sp)
            \\{[load]s} x27, 27*{[regbytes]d}(sp)
            \\{[load]s} x28, 28*{[regbytes]d}(sp)
            \\{[load]s} x29, 29*{[regbytes]d}(sp)
            \\{[load]s} x30, 30*{[regbytes]d}(sp)
            \\{[load]s} x31, 31*{[regbytes]d}(sp)
            \\{[load]s} x2, 2*{[regbytes]d}(sp)
            \\.endm
            \\
        , .{
            .regbytes = rv.asmCmd.regbytes,
            .load = rv.asmCmd.load,
            .store = rv.asmCmd.store,
        }));

    asm (
        \\ .section .text
        \\.globl __alltraps
        \\.align(2)
        \\__alltraps:
        \\SAVE_ALL
        \\move a0,sp
        \\jal trap
        \\.globl  __trapret
        \\__trapret:
        \\LOAD_ALL
        \\sret
        \\
    );
}

extern fn __alltraps() void;
extern fn __trapret() void;

const PushRegs = packed struct {
    zero: usize,
    ra: usize,
    sp: usize,
    gp: usize,
    tp: usize,
    t0: usize,
    t1: usize,
    t2: usize,
    s0: usize,
    s1: usize,
    a0: usize,
    a1: usize,
    a2: usize,
    a3: usize,
    a4: usize,
    a5: usize,
    a6: usize,
    a7: usize,
    s2: usize,
    s3: usize,
    s4: usize,
    s5: usize,
    s6: usize,
    s7: usize,
    s8: usize,
    s9: usize,
    s10: usize,
    s11: usize,
    t3: usize,
    t4: usize,
    t5: usize,
    t6: usize,
};
const TrapFrame = packed struct {
    gpr: PushRegs,
    status: usize,
    epc: usize,
    badvaddr: usize,
    cause: usize,
};

pub fn init() void {
    rv.write_csr("sscratch", 0);
    rv.write_csr("stvec", @intFromPtr(&__alltraps));
}

inline fn exception_handler(fram: *TrapFrame) void {
    switch (fram.cause) {
        rv.ExceptionCause.BREAKPOINT => {
            fram.epc += 2;
            stdio.print("get breakpoint exception from 0x{x}, return to 0x{x}\n", .{ fram.epc - 2, fram.epc });
        },
        rv.ExceptionCause.LOAD_PAGE_FAULT => {},
        rv.ExceptionCause.STORE_PAGE_FAULT => {},
        else => {},
    }
}

var tick: usize = 0;
inline fn interrupt_handler(fram: *TrapFrame) void {
    switch ((fram.cause << 1) >> 1) {
        rv.IRQType.S_TIMER => {
            tick += 1;
            if (tick % 100 == 0) {
                stdio.print("clock tick: {d}\n", .{clock.getCycles()});
            }
            clock.setNextEvent();
        },
        else => {},
    }
}

pub fn trap(fram: *TrapFrame) callconv(.C) void {
    if (@as(isize, @bitCast(fram.cause)) < 0) {
        interrupt_handler(fram);
    } else {
        exception_handler(fram);
    }
}

comptime {
    @export(trap, .{ .name = "trap" });
}
