const rv = @import("../libs/riscv.zig");
const sbi = @import("../libs/sbi.zig");
const stdio = @import("../libs/stdio.zig");

const timebase = 100000;

pub fn init() void {
    _ = rv.set_csr("sie", rv.MIP.STIP);
    setNextEvent();
}

pub fn getCycles() usize {
    const cycles = switch (rv.REGBYTES) {
        8 => asm volatile (
            \\rdtime %[ret]
            : [ret] "=r" (-> usize),
        ),
        else => unreachable,
    };
    return cycles;
}

pub fn setNextEvent() void {
    sbi.setTimer(getCycles() + timebase);
}
