const std = @import("std");
const rv = @import("libs/riscv.zig");
const stdio = @import("libs/stdio.zig");
const builtin = @import("builtin");
const irq = @import("driver/irq.zig");
const trap = @import("kern/trap.zig");
const clock = @import("kern/clock.zig");
const pmm = @import("kern/pmm.zig");
const IDE = @import("driver/IDE.zig");

comptime {
    asm (std.fmt.comptimePrint(
            \\.section .text.entry,"ax",%progbits
            \\.globl kern_entry
            \\kern_entry:
            \\    lui     t0, %hi(boot_page_table_sv39)
            \\    li      t1, 0xffffffffc0000000 - 0x80000000
            \\    sub     t0, t0, t1
            \\    srli    t0, t0, 12
            \\    li      t1, 8 << 60
            \\    or      t0, t0, t1
            \\    csrw    satp, t0
            \\    sfence.vma
            \\    li		t1,		1 << 18
            \\    csrr	t0,		sstatus
            \\    or		t0,		t0,		t1
            \\    csrw	sstatus,	t0
            \\    lui		sp,		%hi(bootstacktop)
            \\    lui t0, %hi(kern_init)
            \\    addi t0, t0, %lo(kern_init)
            \\    jr t0
            \\.section .data
            \\  .align {[PageShift]d}
            \\  .global bootstack
            \\  bootstack:
            \\    .space {[StackSize]d}
            \\  .global bootstacktop
            \\  bootstacktop:
            \\.section .data
            \\ .align {[PageShift]d}
            \\ .global boot_page_table_sv39
            \\ boot_page_table_sv39:
            \\    .quad 0
            \\    .quad 0
            \\    .quad (0x80000 << 10) | 0xcf
            \\    .zero 8 * 508
            \\    .quad (0x80000 << 10) | 0xcf # VRWXAD
        , .{ .PageShift = rv.MMLayout.PGSHIFT, .StackSize = rv.MMLayout.KSTACKSIZE }));
}

export fn kern_init() noreturn {
    printKernInfo();

    trap.init();
    clock.init();
    irq.enable();
    pmm.init(pmm.pageManagerType.bestFitManager) catch unreachable;

    asm volatile ("ebreak");
    while (true) {}
}

fn printKernInfo() void {
    stdio.print(
        \\
        \\================= Kernel Infomartion =======================
        \\Hello {s}{d} !
        \\
    , .{
        "riscv",
        rv.REGBYTES * 8,
    });
    stdio.print(
        \\Register bytes: {d}
        \\Boot stack: 0x{x}
        \\Boot stack top: 0x{x}
        \\Current stack pointer: 0x{x}
        \\Kernel end: 0x{x}
        \\===========================================================
        \\
    , .{
        rv.REGBYTES,
        asm volatile ("la %[ret], bootstack"
            : [ret] "=r" (-> usize),
        ),
        asm volatile ("la %[ret], bootstacktop"
            : [ret] "=r" (-> usize),
        ),
        asm volatile (
            \\mv %[ret], sp
            : [ret] "=r" (-> usize),
        ),
        asm volatile (
            \\la %[ret], end
            : [ret] "=r" (-> usize),
        ),
    });
}
// 这样定义的内核入口，为何 bootstacktop 不等于 bootstack + KSTACKSIZE
