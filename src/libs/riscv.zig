const std = @import("std");
const builtin = @import("builtin");

pub const MMLayout = .{
    .KERNBASE = 0xFFFFFFFFC0200000,
    .KMEMSIZE = 0x7E00000,
    .KERNTOP = 0xFFFFFFFFC0200000 + 0x7E00000,
    .PHYSICAL_MEMORY_END = 0x88000000,
    .PHYSICAL_MEMORY_OFFSET = 0xFFFFFFFF40000000,
    .KERNEL_BEGIN_PADDR = 0x80200000,
    .KERNEL_BEGIN_VADDR = 0xFFFFFFFFC0200000,

    .PGSIZE = 4096,
    .PGSHIFT = 12,

    .KSTACKPAGE = 2,
    .KSTACKSIZE = 2 * 4096,

    .DRAM_BASE = 0x80000000,
};

pub inline fn PADDR(vaddr: usize) usize {
    return vaddr - 0xFFFFFFFF40000000;
}

pub const ExceptionCause = struct {
    pub const MISALIGNED_FETCH = 0x00;
    pub const FAULT_FETCH = 0x01;
    pub const ILLEGAL_INSTRUCTION = 0x2;
    pub const BREAKPOINT = 0x3;
    pub const MISALIGNED_LOAD = 0x4;
    pub const FAULT_LOAD = 0x5;
    pub const MISALIGNED_STORE = 0x6;
    pub const FAULT_STORE = 0x7;
    pub const USER_ECALL = 0x8;
    pub const SUPERVISOR_ECALL = 0x9;
    pub const HYPERVISOR_ECALL = 0xa;
    pub const MACHINE_ECALL = 0xb;
    pub const FETCH_PAGE_FAULT = 0xc;
    pub const LOAD_PAGE_FAULT = 0xd;
    pub const STORE_PAGE_FAULT = 0xf;
};

pub const IRQType = struct {
    pub const U_SOFT = 0;
    pub const S_SOFT = 1;
    pub const H_SOFT = 2;
    pub const M_SOFT = 3;
    pub const U_TIMER = 4;
    pub const S_TIMER = 5;
    pub const H_TIMER = 6;
    pub const M_TIMER = 7;
    pub const U_EXT = 8;
    pub const S_EXT = 9;
    pub const H_EXT = 10;
    pub const M_EXT = 11;
    pub const COP = 12;
    pub const HOST = 13;
};
pub const MIP = struct {
    pub const STIP = (1 << IRQType.S_TIMER);
};

pub const REGBYTES = switch (builtin.target.cpu.arch) {
    .riscv32, .riscv64 => @sizeOf(usize),
    else => @compileError("Unsupported RISC-V architecture"),
};

const ASM = struct {
    regbytes: usize,
    load: []const u8,
    store: []const u8,
};

pub const asmCmd: ASM = switch (REGBYTES) {
    8 => ASM{
        .regbytes = 8,
        .load = "ld",
        .store = "sd",
    },
    else => ASM{
        .regbytes = 4,
        .load = "lw",
        .store = "sw",
    },
};

pub inline fn write_csr(reg: []const u8, val: usize) void {
    if (std.zig.c_builtins.__builtin_constant_p(val) != 0 and val < 32) {
        asm volatile ("csrw " ++ reg ++ ", %[arg0]"
            :
            : [arg0] "i" (val),
        );
    } else {
        asm volatile ("csrw " ++ reg ++ ", %[arg0]"
            :
            : [arg0] "r" (val),
        );
    }
}

pub inline fn set_csr(reg: []const u8, bit: usize) usize {
    var ret: usize = 0;
    if (std.zig.c_builtins.__builtin_constant_p(bit) != 0 and bit < 32) {
        asm volatile ("csrrs $[ret]," ++ reg ++ ", %[arg0]"
            : [ret] "=r" (ret),
            : [arg0] "i" (bit),
        );
    } else {
        asm volatile ("csrrs %[ret]," ++ reg ++ ", %[arg0]"
            : [ret] "=r" (ret),
            : [arg0] "r" (bit),
        );
    }
    return ret;
}

pub inline fn clear_csr(reg: []const u8, bit: usize) void {
    if (std.zig.c_builtins.__builtin_constant_p(bit) != 0 and bit < 32) {
        _ = asm volatile ("csrrc %[ret], " ++ reg ++ ", %[arg0]"
            : [ret] "=r" (-> usize),
            : [arg0] "i" (bit),
        );
    } else {
        _ = asm volatile ("csrrc %[ret], " ++ reg ++ ", %[arg0]"
            : [ret] "=r" (-> usize),
            : [arg0] "r" (bit),
        );
    }
}

pub inline fn read_csr(reg: []const u8) usize {
    return asm volatile ("csrr %[ret], " ++ reg
        : [ret] "=r" (-> usize),
    );
}
