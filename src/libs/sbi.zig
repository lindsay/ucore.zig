const sbiType = enum {
    SetTimer,
    ConsolePutChar,
    ConsoleGetChar,
    ClearIPI,
    SendIPI,
    RemoteFenceI,
    RemoteSFenceVMA,
    RemoteSFenceVMAASID,
    Shutdown,
};

fn call(sbi_type: usize, arg0: usize, arg1: usize, arg2: usize) usize {
    return asm volatile (
        \\mv x17, %[sbi_type]
        \\mv x10, %[arg0]
        \\mv x11, %[arg1]
        \\mv x12, %[arg2]
        \\ecall
        : [ret] "={x10}" (-> usize),
        : [sbi_type] "r" (sbi_type),
          [arg0] "r" (arg0),
          [arg1] "r" (arg1),
          [arg2] "r" (arg2),
        : "memory"
    );
}

pub fn consolePutchar(ch: u8) void {
    _ = call(@intFromEnum(sbiType.ConsolePutChar), ch, 0, 0);
}

pub fn setTimer(stimeVal: usize) void {
    _ = call(@intFromEnum(sbiType.SetTimer), stimeVal, 0, 0);
}

pub extern fn consoleGetchar() u8;
