pub const ListEntry = struct {
    next: *ListEntry,
    prev: *ListEntry,

    pub fn init(self: *ListEntry) void {
        self.prev = self;
        self.next = self;
    }

    pub inline fn addAfter(self: *ListEntry, elm: *ListEntry) void {
        listAdd(elm, self, self.next);
    }

    pub inline fn addBefore(self: *ListEntry, elm: *ListEntry) void {
        listAdd(elm, self.prev, self);
    }
    pub inline fn delete(self: *ListEntry) void {
        listDel(self.prev, self.next);
    }
    pub inline fn isEmpty(self: *ListEntry) bool {
        return self.next == self;
    }
};

inline fn listAdd(elm: *ListEntry, prev: *ListEntry, next: *ListEntry) void {
    prev.next = elm;
    next.prev = elm;
    elm.next = next;
    elm.prev = prev;
}

inline fn listDel(prev: *ListEntry, next: *ListEntry) void {
    prev.next = next;
    next.prev = prev;
}
