const std = @import("std");
const fmt = std.fmt;
const cons = @import("../driver/console.zig");

const ConsoleError = error{};

const Writer = std.io.Writer(void, ConsoleError, stdioCallback);

fn stdioCallback(context: void, str: []const u8) ConsoleError!usize {
    _ = context;
    cons.writeBytes(str);
    return str.len;
}

pub fn print(comptime str: []const u8, args: anytype) void {
    fmt.format(Writer{ .context = {} }, str, args) catch unreachable;
}
