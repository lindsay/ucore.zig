const std = @import("std");

const Self = @This();
const SECSIZE = 512;
const MAX_DISK_NSECS = 56;

ide: [SECSIZE * MAX_DISK_NSECS]u8 = undefined,

const IDEError = error{};

pub fn init(self: *Self) void {
    _ = self;
}
pub fn readSection(self: *Self, secno: u32, dst: []u8) IDEError!usize {
    const iobase = secno * SECSIZE;
    const size: usize = dst.len;
    @memcpy(dst[0..size], self.ide[iobase..(iobase + size)]);
    return size;
}

pub fn writeSection(self: *Self, secno: u32, src: []const u8) !usize {
    const size: usize = (try std.math.divCeil(usize, src.len, SECSIZE)) * SECSIZE;
    const iobase = secno * SECSIZE;
    @memcpy(self.ide[iobase..src.len], src);
    if (src.len < size) {
        @memset(self.ide[src.len..], 0);
    }
    return size;
}

test "test ide io" {
    const assert = std.debug.assert;
    var i: Self = undefined;
    i.init();
    const src = "this is test";
    assert(try i.writeSection(0, src) == SECSIZE);

    var dst: [src.len]u8 = undefined;
    assert(try i.readSection(0, &dst) == src.len);
    assert(std.mem.eql(u8, src, &dst));
}
