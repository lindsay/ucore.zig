const sbi = @import("../libs/sbi.zig");

pub fn init() !void {}

pub fn serial_intr() void {}
pub fn kbd_intr() void {}

pub fn putc(c: u8) void {
    sbi.consolePutchar(c);
}

pub fn writeBytes(bytes: []const u8) void {
    for (bytes) |b| {
        putc(b);
    }
}

pub fn getc() isize {
    return sbi.consoleGetchar();
}
