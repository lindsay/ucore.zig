const rv = @import("../libs/riscv.zig");
const irq = @import("../driver/irq.zig");

pub fn enable() void {
    _ = rv.set_csr("sstatus", 2);
}

pub fn disable() void {
    rv.clear_csr("sstatus", 2);
}

pub fn save() bool {
    if (rv.read_csr("sstatus") & 2) {
        irq.disable();
        return true;
    }
    return false;
}

pub fn restore(f: bool) void {
    if (f) {
        enable();
    }
}
